\documentclass[xcolor=dvipsnames,mathserif]{beamer}
\definecolor{mycolor}{RGB}{60, 140, 30}
\usecolortheme[named=mycolor]{structure}
\usetheme{default}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage{animate}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{algpseudocode}
\lstset{basicstyle=\ttfamily\footnotesize}

\setbeamertemplate{navigation symbols}{} 
\title[PoSITeams]{PoSITeams\\Positive Systems Intelligent Teams}
\subtitle{an Agent-based Simulator to Support Systems Intelligent Behaviour in Organizations}
\author{Teemu Tiinanen, Juha Törmänen, Raimo P. Hämäläinen, Esa Saarinen}
\institute{Aalto University School of Science, Finland}
\date{June 23, 2015}

\newenvironment{changemargin}[2]{%
  \begin{list}{}{%
    \setlength{\topsep}{0pt}%
    \setlength{\leftmargin}{#1}%
    \setlength{\rightmargin}{#2}%
    \setlength{\listparindent}{\parindent}%
    \setlength{\itemindent}{\parindent}%
    \setlength{\parsep}{\parskip}%
  }%
  \item[]}{\end{list}}
\begin{document}

\begin{frame}
 	\titlepage
\end{frame}

\begin{frame}{Background}
\begin{itemize}
\item Systems Thinking in organizations
\begin{itemize}
\item Organizational learning
\item Peter Senge: The fifth discipline, 1990
\end{itemize}
\item Systems intelligence, Raimo P. Hämäläinen and Esa Saarinen 2004
\item Positive psychology, Barbara Fredrickson 1998
\item Affective computing, Rosalind Picard 1997
\end{itemize}
\end{frame}

\section{Introduction}
\begin{frame}{Introduction}
\begin{itemize}
\item Agent Based Modelling (ABM) is an growing field
\item Applications, e.g.:
\begin{itemize}
\item Information diffusion in social networks
\item Emergence of social phenomena
\item Crowd behaviour (disasters, evacuations)
\item Analysis of markets
\end{itemize}
\item Operations Research - the Science of Better - can also benefit from the agent approach in improving the performance of social systems
\item PoSITeams is a web-based organizational social system simulator with a focus on the effects of positivity.
\end{itemize}
\end{frame}

\begin{frame}{Purpose}
	\begin{itemize}
		\item Engage the user in reflective thought-processes and facilitate seeing the system as a whole
		\item Let the user recognize herself as an active part of the system.
		\item Demonstrate systemic effects of different behavioural and structural changes that can occur in organizations.
		\item Support systems intelligent behaviour in organizations.
		\item A tool for developing systems intelligence.
	\end{itemize}
\end{frame}

\section{Background}
\subsection{Systems intelligence}
\begin{frame}{Systems intelligence}
\begin{itemize}
\item Ability to act intelligently within complex systems involving
interaction and feedback.
\item A systems intelligent person perceives the system as a whole and recognizes herself as an active part of the system, who is both able to affect the state of the system and is reciprocally influenced herself by the system.
\item Introduced in 2004 by Raimo P. Hämäläinen and Esa Saarinen in Aalto University. \nocite{saarinen2007}
\end{itemize}
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.2\textwidth]{img/bbb.png}
		\label{bbb}
	\end{center}
\end{figure}
\end{frame}

\begin{frame}{The eight dimensions of systems intelligence}
	\nocite{beingbetterbetter}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/si_factors.png}
		\label{si_dimensions}
	\end{center}
	\end{figure}
\end{frame}

\subsection{Positivity}
\begin{frame}{Positivity}
\begin{itemize}
		\item Broaden-and-build theory (Barbara Fredrickson, 1998): positive emotions
		\begin{itemize}
			\item broaden awareness
			\item build cognitive, social, psychological, emotional and physical resources. \nocite{fredrickson2001}
			\item increase the ability to cope with negativity \nocite{rozin2001}
			\item can become a positive feedback loop towards emotional well-being
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Positivity ratio (P/N)}
\begin{itemize}
\item High ratio of positive to negative affect (P/N):
\begin{itemize}
\item influences behaviour
\begin{itemize}
\item people with high P/N "flourish"
\end{itemize}
\item increases the performance of social groups:
\begin{itemize}
\item effective organizations, Losada (2004)
\item successful marriages, Gottman (1994)
\nocite{barsade2002, losada2004, gottman1994, fredrickson2013}
\end{itemize}
\item increases the number of strong connections in the team
\end{itemize}
\end{itemize}
\end{frame}


\section{Emotional Contagion Model}
\nocite{bosse2009, bosse2009spiral, hoogendoorn2011}
\begin{frame}{Emotional Contagion Model}
\begin{align}
P_j(t+1) &= a_jP_j(t) + b_j + (1 - \beta_j) \sum_{i \neq j} \gamma_{i,j}  P_i^{rel}(t)\\
N_j(t+1) &= c_jN_j(t) + d_j + \beta_j \sum_{i \neq j} \gamma_{i,j}  N_i^{rel}(t)
\end{align}
\begin{itemize}
\item $P$, $N$ positivity and negativity of the agent
\item $\beta$ negativity/positivity bias
\item $\gamma_{i,j} = \varepsilon_i \, \alpha_{i,j} \, \delta_j$ emotional contagion strength between agents i and j, where
\begin{itemize}
\item $\varepsilon_i$ how strongly the agent $i$ expresses its level of emotion
\item $\alpha_{i,j}$ social connection strength between agents $i$ and $j$
\item $\delta_j$ how easily the emotions of agent $j$ are affected by the emotions of others
\end{itemize}
\item $P_j^{rel} = 1 - N_j^{rel} = \frac{P_j}{P_j + N_j}$ relative positivity/negativity
\end{itemize}
\end{frame}

\begin{frame}{Broaden-and-build extension}
\begin{itemize}
\item High P/N increases resilience towards negative events: \nocite{rozin2001}
\end{itemize}
\begin{align}
\beta_j(t) &= P_j^{rel}(t - 1)(\beta_j^{min} - \beta_j^{max}) - \beta_j^{min} + 1
\end{align}
\begin{itemize}
\item High P/N also increases connectivity, sociability and the feeling of "oneness":
\end{itemize}
\begin{align}
\delta_{j}(t) &=  P_j^{rel}(t - 1)(\delta_j^{max} - \delta_j^{min}) + \delta_j^{min}\\
\varepsilon_{j}(t) &=  P_j^{rel}(t - 1)(\varepsilon_j^{max} - \varepsilon_j^{min}) + \varepsilon_j^{min}
\end{align}
\end{frame}

\section{Interactive use of PoSITeams}
\begin{frame}{Interactive use of PoSITeams}
\begin{itemize}
	\item Agents and their connections can be added, removed and modified.
	\item The characteristics of an agent is adjusted by:
	\begin{itemize}
		\item General positivity, P/N in the uninfluenced steady state.
		\item Extroversion $\varepsilon_j$
		\item Emotional sensitivity $\delta_j$
		\item Negativity bias $\beta_j$
		\item Social connection strengths $\alpha_{i,j}$
	\end{itemize}
	\item The whole group or its subgroups can be optimized.
	\item Allowed parameter ranges and costs of changing behaviour can be specified.
\end{itemize}
\end{frame}

\begin{frame}
%%\animategraphics[controls, autoplay, loop, width=\linewidth]{14}{img/animation/anim-}{0}{732}
\end{frame}

\begin{frame}{Using PoSITeams}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{img/use_positeams.pdf}
		\label{use_positeams}
	\end{center}
	\end{figure}
\end{frame}

\section{Improving team behaviour}
\begin{frame}{Improving team behaviour}
\begin{itemize}
	\item Simulator can suggest systems intelligent actions.
	\item Optimization of agent and team parameters
	\begin{itemize}
		\item solution by simulated annealing.
	\end{itemize}
	\item The user can add constraints to model parameters.
	\item Costs can be assigned to making a change in the behavioural parameters.
	\item Reflects the human mental effort of change
\end{itemize}
\end{frame}

\subsection{Simulation results}
\begin{frame}{Improve agent's personal behaviour}
Initial configuration
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex1a.png}
		\label{ex1a}
	\end{center}
	\end{figure}

\end{frame}

\begin{frame}{Improve agent's personal behaviour}
Reaching steady state
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex1b.png}
		\label{ex1b}
	\end{center}
	\end{figure}

\end{frame}

\begin{frame}{Improve agent's personal behaviour}
Optimizing Aapeli:
\begin{itemize}
\item emotional sensitivity 0.8 $\Rightarrow$ 0, extroversion 0.8 $\Rightarrow$ 1
\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex1c_100.png}
		\label{ex1c}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Improve agent's personal behaviour}
Re-optimizing Aapeli:
\begin{itemize}
\item emotional sensitivity 0 $\Rightarrow$ 1
\item solution close to initial negative steady state
\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex1d_100.png}
		\label{ex1d}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Improve team behaviour}
	\begin{itemize}
	\item Cecilia is the leader of two teams, A and B
	\item different agent characteristics, Antti and Cecilia are the most positive and negative agents
	\item strong connections within and weak between teams enforced
	\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex3a.png}
		\label{ex3a}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Improve team behaviour}
	Optimizing all the agent and team parameters
	\begin{itemize}
	\item minimizing negativity bias, maximizing general positivity, strong connections
	\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex3b_100.png}
		\label{ex3b}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Improve team behaviour}
	Change of behaviour requires effort
	\begin{itemize}
	\item set costs for changing behaviour
\end{itemize}
	Optimization with costs
	\begin{itemize}
	\item Antti has a central role in spreading his positivity
	\item Cecilia's role is diminished
	\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex3_tradeoff50_run2.png}
		\label{ex3c}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Improve team structure}
	Optimizing only the connections between agents
	\begin{itemize}
	\item connections from Cecilia minimized, avoid spreading negativity
	\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/sim_ex3_connections.png}
		\label{ex3a}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Adding a new team member}
	Find the ideal role and characteristics of a new team member.
	\begin{itemize}
	\item Boris is added as a new "pseudo member" to team B and optimized
	\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/boris.png}
		\label{ex3a}
	\end{center}
	\end{figure}
\end{frame}

\begin{frame}{Adding a new team member}
	Optimizing Boris
	\begin{itemize}
	\item high general positivity and extroversion, low negativity bias and emotional sensitivity
	\item strong outgoing and weak incoming connections
	\end{itemize}
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{img/boris2.png}
		\label{ex3a}
	\end{center}
	\end{figure}
\end{frame}

\section{Future work}
\begin{frame}{Future work}
	\begin{itemize}
		\item Testing the simulator in real-world organizations
		\item Evaluate if PoSITeams can improve SI in real life
		\item Validation of the emotional contagion model
		\item Further development of the optimization routines and the simulator software
	\end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{References}
	\tiny{\bibliographystyle{abbrv}
	\bibliography{articles}}
\end{frame}

\begin{frame}{Thank you for your attention!}
    	\begin{block}{Try the simulator at}
			\center{\url{http://systemsintelligence.aalto.fi/positeams}}
		\end{block}
\end{frame}

\end{document}