%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multiplicative domain poster
% Created by Nathaniel Johnston
% August 2009
% http://www.nathanieljohnston.com/2009/08/latex-poster-template/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[final]{beamer}
\usepackage[scale=1.5]{beamerposter}
\usepackage{graphicx}			% allows us to import images
\usepackage[utf8]{inputenc}
\usepackage[square,sort&compress,numbers]{natbib}
\usepackage{url}
\def\bibfont{\tiny}

%-----------------------------------------------------------
% Custom commands that I use frequently
%-----------------------------------------------------------

\newcommand{\bb}[1]{\mathbb{#1}}
\newcommand{\cl}[1]{\mathcal{#1}}
\newcommand{\fA}{\mathfrak{A}}
\newcommand{\fB}{\mathfrak{B}}
\newcommand{\Tr}{{\rm Tr}}
\newtheorem{thm}{Theorem}
\newcommand\myheading[1]{%
  \par\smallskip
  {\normalfont\bfseries\color{jblue}#1}\smallskip}

%-----------------------------------------------------------
% Define the column width and poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% The separation I chose is 0.024 and I want 4 columns
% Then set onecolwid to be (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be 2*onecolwid + sepwid = 0.464
%-----------------------------------------------------------

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\setlength{\paperwidth}{117cm}
\setlength{\paperheight}{170cm}
\setlength{\textwidth}{115cm}
\setlength{\textheight}{170cm}
\setlength{\sepwid}{0.024\paperwidth}
\setlength{\onecolwid}{0.22\paperwidth}
\setlength{\twocolwid}{0.464\paperwidth}
\setlength{\topmargin}{-0.5in}
\usetheme{confposter}
\usepackage{exscale}

%-----------------------------------------------------------
% The next part fixes a problem with figure numbering. Thanks Nishan!
% When including a figure in your poster, be sure that the commands are typed in the following order:
% \begin{figure}
% \includegraphics[...]{...}
% \caption{...}
% \end{figure}
% That is, put the \caption after the \includegraphics
%-----------------------------------------------------------

\usecaptiontemplate{
\small
\structure{\insertcaptionname~\insertcaptionnumber:}
\insertcaption}

%-----------------------------------------------------------
% Define colours (see beamerthemeconfposter.sty to change these colour definitions)
%-----------------------------------------------------------

\setbeamercolor{block title}{fg=jblue,bg=white}
\setbeamercolor{block body}{fg=black,bg=white}
\setbeamercolor{block alerted title}{fg=white,bg=jblue}
\setbeamercolor{block alerted body}{fg=black,bg=dred}

%-----------------------------------------------------------
% Name and authors of poster/paper/research
%-----------------------------------------------------------

\title{PoSITeams - Positive Systems Intelligent Teams \\ \large an Agent-Based Simulator for Studying Group Behaviour}
\author{Teemu Tiinanen, Juha Törmänen, Raimo P. Hämäläinen, and Esa Saarinen}
\institute{Aalto University School of Science}

%-----------------------------------------------------------
% Start the poster itself
%-----------------------------------------------------------
% The \rmfamily command is used frequently throughout the poster to force a serif font to be used for the body text
% Serif font is better for small text, sans-serif font is better for headers (for readability reasons)
%-----------------------------------------------------------

\begin{document}
\begin{frame}[t]

	\begin{columns}[t]												% the [t] option aligns the column's content at the top
	
    	\begin{column}{\sepwid}\end{column}			% empty spacer column
			\begin{column}{\twocolwid}							% create a two-column-wide column and then we will split it up later
			\vspace{-2.5cm}

      		\begin{columns}[t,totalwidth=\twocolwid]
	        	\begin{column}{\onecolwid}	
				\vspace{1cm}
				\begin{alertblock}{}
					\begin{itemize}
						\item {\color{Maroon} Background:}
						\begin{itemize}
							\item Systems intelligence
							\item Positivity
						\end{itemize}
						\item {\color{Maroon}Simulation:}
						\begin{itemize}
							\item describe social behaviour in teams
							\item test the effects of behavioural assumptions
							\item test the effects of behavioural changes
						\end{itemize}
						\item {\color{Maroon}Optimization:}
						\begin{itemize}
							\item help teams to flourish
							\item find better structures
							\item find better personal behaviour
						\end{itemize}
						\item {\color{Maroon}Web-based implementation:}
						\begin{itemize}
							\item allows easy distribution of the application
						\end{itemize}
					\end{itemize}
				\end{alertblock}
				\vskip2ex
				
	    		\begin{block}{Systems Intelligence}
					\begin{itemize}
						\item \textbf{Ability to act intelligently within complex systems involving interaction and feedback.} \nocite{saarinen2007}
						\item A systems intelligent person perceives the system as a whole and recognizes herself as an active part of the system, who is both able to affect the state of the system and is reciprocally influenced herself by the system.
					\end{itemize}
					\vspace{1cm}
					\begin{figure}[H]
						\begin{center}
					    	\includegraphics[width=\textwidth]{img/si_factors.png}
					      	\caption{The eight dimensions of systems intelligence. \nocite{beingbetterbetter}}
					  		\label{si_dimensions}
					 	\end{center}
					\end{figure}
					\vspace{1cm}
					\begin{figure}[H]
						\begin{minipage}{0.45\textwidth}
	  						\begin{center}
	      						\includegraphics[width=\textwidth]{img/bbb.png}
	  							\label{bbb}
							\end{center}
			 			\end{minipage}
						\begin{minipage}{0.45\textwidth}
	  						\begin{center}
	      						\includegraphics[width=\textwidth]{img/positivity.jpg}
	  							\label{positivity}
	 						\end{center}
						\end{minipage}
					\end{figure}
				\end{block}
				\vskip1ex
				
			    \begin{block}{Positivity}
					\begin{itemize}
						\item Positive emotions:
						\begin{itemize}
							\item build cognitive, social, psychological, emotional and physical resources. \nocite{fredrickson2001}
							\item increase the ability to cope with negativity \nocite{rozin2001}
							\item can become a positive feedback loop towards emotional well-being
						\end{itemize}
					\end{itemize}
					\begin{itemize}
						\item High positivity ratios:
						\begin{itemize}
							\item increase the performance of social groups and individuals \nocite{barsade2002, losada2004, gottman1994, fredrickson2013}
							\item increase the number of strong connections in the team
						\end{itemize}
					\end{itemize}
	    		\end{block}
	    	\end{column}
	    	
			\begin{column}{\sepwid}\end{column}			% empty spacer column
	  		\begin{column}{\onecolwid}
	     		\begin{block}{Purpose}
					\begin{itemize}
						\item {\color{Maroon}Engage the user in reflective thought-processes} and facilitate seeing the system as a whole and let the user recognize herself as an active part of the system.
						\vspace{0.5cm}
						\item Demonstrate systemic effects of different behavioural and structural changes that can occur in organizations.
						\vspace{0.5cm}
						\item {\color{Maroon}Support systems intelligent behaviour in organizations.}
						\vspace{0.5cm}
						\item A tool for developing systems intelligence.
					\end{itemize}
	     		\end{block}
	     		\vskip4ex
	     		
	        	\begin{block}{Improving team behaviour}
					\begin{itemize}
						\item Optimization of model parameters with simulated annealing.
						\vspace{0.3cm}
						\item Simulator can suggest systems intelligent actions.
						\vspace{0.3cm}
						\item The user can add constraints to model parameters.
						\vspace{0.3cm}
						\item Costs can be assigned to changing the behavioural parameters.
					\end{itemize}
	        	\end{block}\vskip4.3ex
	        	
				\begin{block}{Using PoSITeams}
					\begin{itemize}
						\item Agents and their connections can be added, removed and modified.
						\vspace{0.5cm}
						\item The behaviour of an agent can be adjusted by:
						\vspace{0.3cm}
						\begin{itemize}
							\item General positivity, P/N in the uninfluenced steady state.
							\vspace{0.1cm}
							\item Extroversion $\varepsilon_j$
							\vspace{0.1cm}
							\item Emotional sensitivity $\delta_j$
							\vspace{0.1cm}
							\item Negativity bias $\beta_j$
							\vspace{0.1cm}
							\item Social connection strengths $\alpha_{i,j}$
						\end{itemize}
						\vspace{0.3cm}
						\item The whole group or its subgroups can be optimized.
						\vspace{0.3cm}
						\item Allowed parameter ranges and costs of changing behaviour can be specified.
					\end{itemize}
				\end{block}
				\vskip4.2ex
				
				\begin{block}{Future work}
					\begin{itemize}
						\item Testing the simulator in a real-world organization.
						\vspace{0.3cm}
						\item Validation of the emotional contagion model.
						\vspace{0.3cm}
						\item Evaluate if SI can be improved in real life by PoSITeams.
						\vspace{0.3cm}
						\item Further development of the simulator software.
					\end{itemize}
			     \end{block}
			     
	        \end{column}
	    \end{columns}
	    \vspace{2cm}
    	\begin{block}{Try the simulator at}
			\center{{\color{Maroon}\Large\textbf{\url{http://systemsintelligence.aalto.fi/positeams}}}}
		\end{block}
		\end{column}

  		\begin{column}{\sepwid}\end{column}			% empty spacer column
		\begin{column}{\twocolwid}							% create a two-column-wide column and then we will split it up later
		\vspace{-2.5cm}

      		\begin{columns}[t,totalwidth=\twocolwid]
	        \begin{column}{\onecolwid}
    		    \begin{block}{Improve personal behaviour}
					\begin{figure}[H]
						\begin{center}
							\includegraphics[width=\textwidth]{img/sim_ex1b.png}
							\label{ex1}
 						\end{center}
					\end{figure}
					\begin{align*}
						\scalebox{2.5}{$\Downarrow$}
					\end{align*}
					\begin{figure}[H]
  						\begin{center}
      						\includegraphics[width=\textwidth]{img/sim_ex1c_100.png}
      						\caption{The team after optimizing the behaviour of Aapeli, emotional sensitivity: 0.8 $\Rightarrow$ 0, extroversion: 0.8 $\Rightarrow$ 1.}
  							\label{ex1b}
 						\end{center}
					\end{figure}
        		\end{block}
        	\end{column}
        
	        \begin{column}{\onecolwid}
	        	\begin{block}{Improve team structure}
					\begin{figure}[H]
		  				\begin{center}
	      					\includegraphics[width=\textwidth]{img/sim_ex3a.png}
	  						\label{ex2}
	 					\end{center}
					\end{figure}
					\begin{align*}
						\scalebox{2.5}{$\Downarrow$}
					\end{align*}
					\begin{figure}[H]
	  					\begin{center}
	      					\includegraphics[width=\textwidth]{img/sim_ex3_connections.png}
	      					\caption{The team with an optimized structure.}
	  						\label{ex2b}
	 					\end{center}
					\end{figure}
	        	\end{block}
	      	\end{column}
			\end{columns}
			
			\vskip4ex
      		\begin{block}{Adding a new team member}	% an ACTUAL two-column-wide column
				\begin{figure}[H]
					\begin{minipage}{0.45\textwidth}
  						\begin{center}
      						\includegraphics[width=\textwidth]{img/boris.png}
  							\label{ex3}
						\end{center}
		 			\end{minipage}
					\begin{minipage}{0.05\textwidth}
						\begin{align*}
							\scalebox{2.5}{$\Rightarrow$}
						\end{align*}
					\end{minipage}
					\begin{minipage}{0.45\textwidth}
  						\begin{center}
      						\includegraphics[width=\textwidth]{img/boris2.png}
  							\label{ex3b}
 						\end{center}
					\end{minipage}
				\end{figure}
      		\end{block}
			\vskip2ex
			
			\begin{block}{Model}
				\nocite{bosse2009, bosse2009spiral, hoogendoorn2011}
				\begin{minipage}{0.5\textwidth}
				
				
        		\myheading{Emotional contagion}
        			\begin{align*}
					P_j(t+1) &= aP_j(t) + b + \sum_{i \neq j} I_{i,j}^P(t)\\
					N_j(t+1) &= cN_j(t) + d + \sum_{i \neq j} I_{i,j}^N(t)
				\end{align*}
				\vspace{-1.5cm}
				\myheading{Influence functions}
				\begin{align*}
					I_{i,j}^P(t) &= \gamma_{i,j} (1 - \beta_j) P_i^{rel}(t) \\ 
					I_{i,j}^N(t) &= \gamma_{i,j} \beta_j N_i^{rel}(t)
				\end{align*}
				\myheading{Relative positivity}
				\begin{align*}
				P_j^{rel} = 1 - N_j^{rel} = \frac{P_j}{P_j + N_j}
				\end{align*}
				\end{minipage}
				\begin{minipage}{0.5\textwidth}
				\begin{itemize}
				\item $P_j$, $N_j$ level of positivity and negativity
				\item $\beta_j$ negativity bias
				\vspace{0.3cm}
				\item $\gamma_{i,j} = \varepsilon_i \, \alpha_{i,j} \, \delta_j$ emotional contagion strength between agents $i$ and $j$, where
					\vspace{0.3cm}
					\begin{itemize}
						\item $\varepsilon_i$ how strongly the agent $i$ expresses its level of emotion
						\item $\alpha_{i,j}$ social connection strength between agents $i$ and $j$
						\item $\delta_j$ how easily the emotions of agent $j$ are affected by the emotions of others
					\end{itemize}
				\end{itemize}
				
				\end{minipage}
				\vspace{1cm}
				\myheading{Broaden-and-build extension}
				\begin{itemize}			
					\item Increases connectivity and ability to cope with negativity as P/N increases.
				\end{itemize}
				\begin{align*}
					\delta_{j}(t) &=  P_j^{rel}(t - 1)(\delta_j^{max} - \delta_j^{min}) + \delta_j^{min}\\
					\varepsilon_{j}(t) &=  P_j^{rel}(t - 1)(\varepsilon_j^{max} - \varepsilon_j^{min}) + \varepsilon_j^{min}\\
					\beta_j(t) &= P_j^{rel}(t - 1)(\beta_j^{min} - \beta_j^{max}) - \beta_j^{min} + 1
				\end{align*}
				
    		\end{block}
			\vskip2ex
		    \begin{block}{References}
				\tiny{\bibliographystyle{abbrv}
				\bibliography{articles}}
    			\end{block}
  		\end{column}
		\begin{column}{\sepwid}\end{column}			% empty spacer column
 	\end{columns}
\end{frame}
\end{document}